    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // add a movie to the favorites
            $('body').on('click', '.add-movie-to-favorites', function () {
                let movie_id = $(this).data("id");
                let vote_average = $(this).data("vote-average");
                let url = "{{ url('/ajax/favorites/add/movie/{movie_id}/vote_average/{vote_average}')}}";
                console.log("->add-movie-to-favorites");
                $.ajax({
                    url: url.replace('{movie_id}', movie_id).replace('{vote_average}', vote_average),
                    success: function (data) {
                        console.log("add-movie-to-favorites = " + movie_id);
                        $('.add-movie-to-favorites[data-id="' + movie_id + '"]').hide();
                        $('.remove-movie-from-favorites[data-id="' + movie_id + '"]').show();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });

            // remove a movie from the favorites
            $('body').on('click', '.remove-movie-from-favorites', function () {
                let movie_id = $(this).data("id");
                console.log("->remove-movie-from-favorites");
                $.ajax({
                    url: "{{ url('/ajax/favorites/delete/movie')}}"+'/'+movie_id,
                    success: function (data) {
                        console.log("remove-movie-from-favorites = " + movie_id);
                        $('.add-movie-to-favorites[data-id="' + movie_id + '"]').show();
                        $('.remove-movie-from-favorites[data-id="' + movie_id + '"]').hide();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });
        });
    </script>
