@extends('layout')

@section('content')
    @inject('image', 'Tmdb\Helper\ImageHelper')

    <div class="row my-favorite-movies">Movies</div>
    <div class="row text-white pb-3">100 movies with the most votes (click on the movie for more information)</div>

    {{ $movies->links() }}

    <div class="row">
    @foreach ($movies as $movie)
        <div class="col">
            <div class="item-image">
                <a href="/movie/{{ $movie->getId() }}">
                    {!! $image->getHtml($movie->getPosterImage(), 'w185', 185, 278) !!}
                </a>
            </div>
            <div class="item-text">
                <div class="movies-title"><a href="/movie/{{ $movie->getId() }}">{{ $movie->getTitle() }}</a></div>
                <div class="row moview-row">
                    <div class="movies-rating"><span style="color: yellow;"><i class="fas fa-star"></i></span> {{ $movie->getVoteAverage() }}/10</div>
                    <div class="movies-favorite">
                    @guest
                    @else
                        <i class="fas fa-heart remove-movie-from-favorites text-danger" data-id="{{ $movie->getId() }}"></i>
                        <i class="far fa-heart add-movie-to-favorites" data-id="{{ $movie->getId() }}" data-vote-average="{{ $movie->getVoteAverage() }}"></i>
                        <script>
                            @if (in_array($movie->getId(), $favorites))
                            $('.add-movie-to-favorites[data-id="{{ $movie->getId() }}"]').hide();
                            @else
                            $('.remove-movie-from-favorites[data-id="{{ $movie->getId() }}"]').hide();
                            @endif
                        </script>
                    @endguest
                    </div>
                    <div class="movies-year">{!! $movie->getReleaseDate()->format('Y') !!}</div>
                </div>
            </div>
        </div>
    @endforeach
    </div>

    {{ $movies->links() }}
@endsection

@section('afterbody')
    @include('favorites-script');
@endsection