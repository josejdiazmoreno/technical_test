@extends('layout')

@section('content')
@inject('image', 'Tmdb\Helper\ImageHelper')

    <div class="row top-movies">Top Movies</div>
    <div class="row text-white pb-3">more than 4-star average rating, no more than 10% negative votes in 2019, and does not star Keanu Reeves</div>
    {{ $movies->links() }}
    <div class="row">
        @foreach ($movies as $movie)
        <div class="col-movie-favorites">
            <div class="item-image">
                <a href="/movie/{{ $movie['movie_id'] }}">
                    {!! $image->getHtml($movie['poster_image'], 'w185', 185, 278) !!}
                </a>
            </div>
            <div class="item-text">
                <div class="movies-title"><a href="/movie/{{ $movie['movie_id'] }}">{{ $movie['title'] }}</a></div>
                <div class="row moview-row">
                    <div class="movies-rating"><span style="color: yellow;"><i class="fas fa-star"></i></span> {{ $movie['vote_average'] }}/10</div>
                    <div class="movies-favorite">
                        @guest
                        @else
                        <i class="fas fa-heart remove-movie-from-favorites text-danger" data-id="{{ $movie['movie_id'] }}"></i>
                        <i class="far fa-heart add-movie-to-favorites" data-id="{{ $movie['movie_id'] }}" data-vote-average="{{ $movie['vote_average'] }}"></i>
                        <script>
                            @if(in_array($movie['movie_id'], $favorites))
                            $('.add-movie-to-favorites[data-id="{{ $movie['
                                movie_id '] }}"]').hide();
                            @else
                            $('.remove-movie-from-favorites[data-id="{{ $movie['
                                movie_id '] }}"]').hide();
                            @endif
                        </script>
                        @endguest
                    </div>
                    <div class="movies-year">{!! date('Y', strtotime($movie['release_date'])) !!}</div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

    {{ $movies->links() }}
@endsection

@section('afterbody')
@include('favorites-script');
@endsection