@extends('layout')

@section('content')
    @inject('image', 'Tmdb\Helper\ImageHelper')

    <div class="row my-favorite-movies">My favorite movies</div>
    <div class="row text-white">ordered by average score according to votes received</div>
    <div class="row">
    @foreach ($movies as $movie)
        <div class="col-movie-favorites">
            <div class="item-image">
                <a href="/movie/{{ $movie->getId() }}">
                    {!! $image->getHtml($movie->getPosterImage(), 'w185', 185, 278) !!}
                </a>
            </div>
            <div class="item-text">
                <div class="movies-title"><a href="/movie/{{ $movie->getId() }}">{{ $movie->getTitle() }}</a></div>
                <div class="row moview-row">
                    <div class="movies-rating"><span style="color: yellow;"><i class="fas fa-star"></i></span> {{ $movie->getVoteAverage() }}/10</div>
                    <div class="movies-favorite">
                        <i class="fas fa-heart remove-movie-from-favorites text-danger" data-id="{{ $movie->getId() }}"></i>
                        <i class="far fa-heart add-movie-to-favorites" data-id="{{ $movie->getId() }}" data-vote-average="{{ $movie->getVoteAverage() }}"></i>
                        <script>
                            $('.add-movie-to-favorites[data-id="{{ $movie->getId() }}"]').hide();
                        </script>
                    </div>
                    <div class="movies-year">{!! $movie->getReleaseDate()->format('Y') !!}</div>
                </div>
            </div>
        </div>
    @endforeach
    </div>
@endsection

@section('afterbody')
    @include('favorites-script');
@endsection
