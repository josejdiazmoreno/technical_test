@extends('layout')

@section('content')
    @inject('image', 'Tmdb\Helper\ImageHelper')
    <div class="row">
        <div class="col">
            <div class="item-image">
                {!! $image->getHtml($movie->getBackdropPath(), 'w1280', 1110, 624) !!}
            </div>
            <div class="item-text">
                <div class="row">
                    @guest
                    @else
                        <div class="movie-favorite">
                            <i class="fas fa-heart remove-movie-from-favorites text-danger"
                               data-id="{{ $movie->getId() }}"></i>
                            <i class="far fa-heart add-movie-to-favorites" data-id="{{ $movie->getId() }}"
                               data-vote-average="{{ $movie->getVoteAverage() }}"></i>
                            <script>
                                @if ($is_favorite)
                                $('.add-movie-to-favorites').hide();
                                @else
                                $('.remove-movie-from-favorites').hide();
                                @endif
                            </script>
                        </div>
                    @endguest
                    <div class="movie-title">{{ $movie->getTitle() }}</div>
                    <div class="movie-ratings">
                        <span class="movie-rating"><span style="color: yellow;"><i class="fas fa-star"></i></span> {{ $movie->getVoteAverage() }}</span><span
                            class="movie-rating-normal">/10</span>
                    </div>
                </div>
                <div class="row">
                    <div class="movie-info">
                        Director: {{ $movie_director }}<span class="spacer">|</span>{{ $movie_genres }}
                        <span class="spacer">|</span>{{ $movie->getReleaseDate()->format('Y') }}<span
                            class="spacer">|</span>{{ date('G\h i\m', mktime(0, $movie->getRuntime())) }}</div>
                    @if ($movie->getTagline())
                        <div class="movie-tagline"><i>{{ $movie->getTagline() }}</i></div>@endif
                    <div class="movie-overview">{{ $movie->getOverview() }}</div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('afterbody')
    @include('favorites-script');
@endsection
