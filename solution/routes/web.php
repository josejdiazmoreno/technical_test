<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DefaultController@movies');
Route::get('/movie/{movie_id}', 'DefaultController@movie');
Route::get('/favorites', 'FavoriteController@index');
Route::get('/top-movies', 'TopMoviesController@index');

Route::get('/ajax/favorites/add/movie/{movie_id}/vote_average/{vote_average}', 'AjaxController@addMovie');
Route::get('/ajax/favorites/delete/movie/{movie_id}', 'AjaxController@deleteMovie');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');
