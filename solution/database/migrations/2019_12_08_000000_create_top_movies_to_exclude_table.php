<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopMoviesToExcludeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('top_movies_to_exclude', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('movie_id');
            $table->string('person_id');
            $table->string('person_name');
            $table->boolean('has_person_in_cast');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('top_movies_to_exclude');
    }
}
