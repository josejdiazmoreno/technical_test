<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopMoviesUnfilteredTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('top_movies_unfiltered', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('movie_id');
            $table->string('title');
            $table->string('vote_average');
            $table->string('poster_image');
            $table->string('release_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('top_movies_unfiltered');
    }
}
