<?php

namespace Tests\Feature\Http\Controllers;

use App\User;
use Tests\TestCase;

class FavoriteControllerTest extends TestCase
{
    /**
     * Test add movie authenticated
     */
    public function testFavoritesAuthenticated()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get('/favorites');

        $response->assertStatus(200);
        $this->assertAuthenticated();
    }

    /**
     * Test add movie NOT authenticated
     */
    public function testFavoritesNotAuthenticated()
    {
        $response = $this->get('/favorites');

        $response->assertStatus(500);
        $this->assertGuest();
    }
}
