<?php

namespace Tests\Feature\Http\Controllers;

use App\User;
use Tests\TestCase;

class AjaxControllerTest extends TestCase
{
    /**
     * Test add movie authenticated
     */
    public function testAddMovieAuthenticated()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get('/ajax/favorites/add/movie/330457/vote_average/9.9');

        $response->assertStatus(200);
        $this->assertAuthenticated();
    }

    /**
     * Test add movie NOT authenticated
     */
    public function testAddMovieNotAuthenticated()
    {
        $response = $this->get('/ajax/favorites/add/movie/330457/vote_average/9.9');

        $response->assertStatus(500);
        $this->assertGuest();
    }

    /**
     * Test delete movie authenticated
     */
    public function testDeleteMovieAuthenticated()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get('/ajax/favorites/delete/movie/453405');

        $response->assertStatus(200);
        $this->assertAuthenticated();
    }

    /**
     * Test delete movie NOT authenticated
     */
    public function testDeleteMovieNotAuthenticated()
    {
        $response = $this->get('/ajax/favorites/delete/movie/453405');

        $response->assertStatus(500);
        $this->assertGuest();
    }
}
