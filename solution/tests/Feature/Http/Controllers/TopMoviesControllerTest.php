<?php

namespace Tests\Feature\Http\Controllers;

use Tests\TestCase;

class TopMoviesControllerTest extends TestCase
{
    /**
     * Test view top movies
     */
    public function testTopMovies()
    {
        $response = $this->get('/top-movies');

        $response->assertStatus(200);
    }
}
