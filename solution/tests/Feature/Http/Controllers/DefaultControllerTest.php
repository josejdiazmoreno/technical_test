<?php

namespace Tests\Feature\Http\Controllers;

use Tests\TestCase;

class DefaultControllerTest extends TestCase
{
    /**
     * Test Movies
     */
    public function testMovies()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Test Movie
     */
    public function testMovie()
    {
        $response = $this->get('/movie/330457');

        $response->assertStatus(200);
    }
}
