<?php

namespace Tests\Feature\Http\Controllers;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HomeControllerTest extends TestCase
{
    /**
     * Test home
     */
    public function testHomeAuthenticated()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get('/home');

        $response->assertStatus(302);
        $this->assertAuthenticated();
    }

    /**
     * Test home
     */
    public function testHomeNotAuthenticated()
    {
        $response = $this->get('/home');

        $response->assertStatus(302);
        $this->assertGuest();
    }

    /**
     * Test home
     */
    public function testAdminAuthenticated()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get('/admin/home');

        $response->assertStatus(500);
        $this->assertAuthenticated();
    }

    /**
     * Test home
     */
    public function testAdminNotAuthenticated()
    {
        $response = $this->get('/admin/home');

        $response->assertStatus(302);
        $this->assertGuest();
    }
}
