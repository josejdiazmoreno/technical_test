<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopMovieToExclude extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'top_movies_to_exclude';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['movie_id', 'person_id', 'person_name', 'has_person_in_cast'];
}
