<?php

namespace App\Http\Controllers;

use App\Favorite;
use Illuminate\Contracts\View\Factory;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Tmdb\Helper\ImageHelper;
use Tmdb\Repository\MovieRepository;

class FavoriteController extends Controller
{
    private $movies;
    private $helper;

    /**
     * FavoriteController constructor.
     * @param MovieRepository $movies
     * @param ImageHelper $helper
     */
    function __construct(MovieRepository $movies, ImageHelper $helper)
    {
        $this->movies = $movies;
        $this->helper = $helper;
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $favorites = Favorite::query()->select(['movie_id'])
            ->where('user_id', '=', Auth::user()->id)
            ->orderBy('vote_average', 'DESC')
            ->pluck('movie_id')
            ->toArray();

        $movies = [];
        foreach ($favorites as $favorite) {
            $movies[] = $this->movies->load($favorite);
        }

        $pageName = 'page';
        $page = Paginator::resolveCurrentPage($pageName);
        $favoritesPaginated = new LengthAwarePaginator($movies, $total = 50, 10, $page, [
            'path'     => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);

        return view('show-favorites', [
            'breadcrumb_movie_title' => false,
            'movies'                 => $favoritesPaginated,
            'page'                   => $page
        ]);
    }
}
