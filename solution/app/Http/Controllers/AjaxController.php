<?php

namespace App\Http\Controllers;

use App\Favorite;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Response;

class AjaxController extends Controller
{
    function addMovie($movie_id, $vote_average)
    {
        $favorite = Favorite::firstOrCreate([
            'movie_id'     => $movie_id,
            'user_id'      => Auth::user()->id,
            'vote_average' => $vote_average
        ]);

        $favorite->save();

        return Response::json($favorite);
    }

    function deleteMovie($movie_id)
    {
        $favorite = Favorite::where('movie_id', $movie_id)
            ->where('user_id', Auth::user()->id);
        $favorite->delete();

        return Response::json($favorite);
    }
}
