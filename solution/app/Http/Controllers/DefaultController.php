<?php

namespace App\Http\Controllers;

use App\Favorite;
use Illuminate\Contracts\View\Factory;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\View\View;
use Tmdb\Helper\ImageHelper;
use Tmdb\Repository\MovieRepository;

class DefaultController extends Controller
{
    private $movies;
    private $helper;

    /**
     * DefaultController constructor.
     * @param MovieRepository $movies
     * @param ImageHelper $helper
     */
    function __construct(MovieRepository $movies, ImageHelper $helper)
    {
        $this->movies = $movies;
        $this->helper = $helper;
    }

    /**
     * @return Factory|View
     */
    public function movies()
    {
        $pageName = 'page';
        $page = Paginator::resolveCurrentPage($pageName);

//        $key = 'movies_page_{$page}';
//        $movies = Redis::get($key);

//        if (!$movies) {
            $movies = $this->movies->getTopRated(['page' => $page]);
//            Redis::set($key, $movies);
//        }

        $moviesPaginated = new LengthAwarePaginator($movies, $total = 100, 20, $page, [
            'path'     => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);

        // extract a list of current favorites
        $favorites = null;
        if (Auth::user()) {
            $favorites = Favorite::query()->select(['movie_id'])->where('user_id', '=',
                Auth::user()->id)->pluck('movie_id')->toArray();
        }

        return view('show-movies', [
            'breadcrumb_movie_title' => false,
            'movies'                 => $moviesPaginated,
            'page'                   => $page,
            'favorites'              => $favorites
        ]);
    }

    /**
     * @param $movie_id
     * @return Factory|View
     */
    public function movie($movie_id)
    {
        $parameters = ['videos,trailers,images,casts,translations'];
        $headers = [];

        $movie = $this->movies->load($movie_id, $parameters, $headers);

        $movie_genres = [];
        foreach ($movie->getGenres() as $genre) {
            $movie_genres[] = $genre->getName();
        }

        $directors = [];
        $crew = $movie->getCredits()->getCrew();

        foreach ($crew as $person) {
            if ($person->getJob() == 'Director') {
                $directors[] = $person->getName();
            }
        }

        $favorite = null;
        if (Auth::user()) {
            $favorite = Favorite::where('movie_id', $movie_id)->where('user_id', Auth::user()->id)->count();
        }

        return view('show-movie', [
            'breadcrumb_movie_title' => true,
            'movie'                  => $movie,
            'movie_genres'           => implode(', ', $movie_genres),
            'movie_director'         => implode(', ', $directors),
            'is_favorite'            => $favorite
        ]);
    }
}
