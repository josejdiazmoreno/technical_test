<?php

namespace App\Http\Controllers;

use App\Favorite;
use App\TopMovie;
use App\TopMovieToExclude;
use App\TopMovieUnfiltered;
use Illuminate\Contracts\View\Factory;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Tmdb\Helper\ImageHelper;
use Tmdb\Model\Query\Discover\DiscoverMoviesQuery;
use Tmdb\Model\Search\SearchQuery\PersonSearchQuery;
use Tmdb\Repository\DiscoverRepository;
use Tmdb\Repository\MovieRepository;
use Tmdb\Repository\SearchRepository;

class TopMoviesController extends Controller
{
    private $movies;
    private $discover;
    private $search;
    private $helper;

    private $person_id;
    private $person_name = 'Keanu Reeves';

    /**
     * TopMoviesController constructor.
     * @param MovieRepository $movies
     * @param DiscoverRepository $discover
     * @param SearchRepository $search
     * @param ImageHelper $helper
     */
    function __construct(
        MovieRepository $movies,
        DiscoverRepository $discover,
        SearchRepository $search,
        ImageHelper $helper
    ) {
        $this->movies = $movies;
        $this->discover = $discover;
        $this->search = $search;

        $this->helper = $helper;
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        // prepare pagination info
        $pageName = 'page';
        $page = Paginator::resolveCurrentPage($pageName);
        $limit = 20;
        $offset = ($page - 1) * $limit;

        // get person_id for the actor to exclude in the listings
        $this->getPersonData();

        // search movies according the specific filter
        $query = new DiscoverMoviesQuery();
        $query
            ->page($page)
            ->voteAverageGte(4)
            ->primaryReleaseDateGte('2019-01-01');
        $movies = $this->discover->discoverMovies($query);

        // store on db all movies not looking at the actor
        $this->storeMoviesUnfiltered($movies);

        // now we store on db movies with the actor we want to exclude
        $query->withCast($this->person_id);
        $moviesWithSelectedActor = $this->discover->discoverMovies($query);
        $this->storeMoviesToExclude($moviesWithSelectedActor, $this->person_id, $this->person_name);

        // let's create the query with Eloquent
        $moviesToExclude = TopMovieToExclude::pluck('movie_id')->all();
        $movies = TopMovieUnfiltered::whereNotIn('movie_id', $moviesToExclude)
            ->select()
            ->offset($offset)
            ->limit($limit)
            ->get();

        // paginate results already excluded about the actor
        $moviesPaginated = new LengthAwarePaginator($movies, $total = 100, $limit, $page, [
            'path'     => Paginator::resolveCurrentPath(),
            'pageName' => $pageName
        ]);

        // extract a list of current favorites
        $favorites = null;
        if (Auth::user()) {
            $favorites = Favorite::query()
                ->select(['movie_id'])
                ->where('user_id', '=', Auth::user()->id)
                ->pluck('movie_id')
                ->toArray();
        }

        return view('top-movies', [
            'breadcrumb_movie_title' => false,
            'movies'                 => $moviesPaginated,
            'page'                   => $page,
            'favorites'              => $favorites
        ]);
    }

    /**
     *
     */
    private function getPersonData()
    {
        $search_person = $this->search
            ->searchPerson($this->person_name, new PersonSearchQuery())
            ->toArray();
        $person = reset($search_person);

        $this->person_id = $person->getId();
    }

    /**
     * Find all the movie ids that match the conditions required,
     * so we can filter and not show them on the listings
     *
     * @param $movies
     */
    protected function storeMoviesUnfiltered($movies)
    {
        foreach ($movies as $movie) {
            $movieToExclude = TopMovieUnfiltered::firstOrCreate([
                'movie_id'     => $movie->getId(),
                'title'        => $movie->getTitle(),
                'vote_average' => $movie->getVoteAverage(),
                'poster_image' => $movie->getPosterImage(),
                'release_date' => $movie->getReleaseDate()
            ]);

            $movieToExclude->save();
        }
    }

    /**
     * Find all the movie ids that match the conditions required,
     * so we can filter and not show them on the listings
     *
     * @param $movies
     * @param $person_id
     * @param $person_name
     */
    protected function storeMoviesToExclude($movies, $person_id, $person_name)
    {
        foreach ($movies as $movie) {
            $movieToExclude = TopMovieToExclude::firstOrCreate([
                'movie_id'           => $movie->getId(),
                'person_id'          => $person_id,
                'person_name'        => $person_name,
                'has_person_in_cast' => true
            ]);

            $movieToExclude->save();
        }
    }
}
