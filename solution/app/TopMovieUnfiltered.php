<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopMovieUnfiltered extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'top_movies_unfiltered';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['movie_id', 'title', 'vote_average', 'poster_image', 'release_date'];
}
