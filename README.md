# PHP Developer Technical Test

![](README_images/9.png)

## Author

Jose Diaz Moreno <josejdiazmoreno@gmail.com>

## Technologies

* PHP 7.3.12
* Laravel 6.2
* PHPUnit
* Docker
* Redis

## What is this document?

I created this document to have useful information quickly located when configuring different parts of the project.

The main focus is on creating the structure in Docker and running unit tests.

## Install Laradock

    git clone https://github.com/Laradock/laradock.git

## Configure Laradock

Create the `.env` file and edit it so that **Laradock** recognizes our **Laravel** project by looking for the `DOCKER_HOST_IP` variable and setting it to 
`127.0.0.1`.

	cd laradock
	cp .env-example .env
	nano .env

Then, edit the value of `DOCKER_HOST_IP`.

	DOCKER_HOST_IP=127.0.0.1

![](README_images/1.png)

## Create the containers

In this case, we will use **nginx** as the server and **mysql** as the database. This command will download the containers if it is the first time. If they are 
already installed, it will only activate them.

	sudo docker-compose up -d nginx mysql workspace phpmyadmin redis

## List our containers

To access them later.

	docker-compose ps

![](README_images/2.png)

## Access our workspace container

This is done in order to later install our project; it is like accessing a machine within our machine.

	sudo docker-compose exec workspace bash

We can modify the default path in the **Docker** `.env` file.

![](README_images/3.png)

## Install Laravel inside our workspace container

We do it in the traditional way, then create the `.env`, run:

	root@0bd8859e9750:/var/www# composer create-project --prefer-dist laravel/laravel technical_test
	root@0bd8859e9750:/var/www# exit
	$ cd ../technical_test/
	$ sudo chmod -R 777 storage bootstrap/cache

## Configure the Nginx container to point to our project

Edit the **Laradock** `.env` file again and place the name of the project folder in the variable.

	APP_CODE_PATH_HOST=../technical_test

![](README_images/4.png)

## Restart the containers

	sudo docker-compose down
	sudo docker-compose up -d nginx mysql workspace phpmyadmin redis

## Configure the Mysql container

Go back to the **Laradock** `.env` file and edit `MYSQL_DATABASE` and `MYSQL_ROOT_PASSWORD`.

_(Replace "dockerapp" with the name of the database you want to use)_

![](README_images/5.png)
￼
Then go to the **mysql** `my.cnf` file as follows:

	nano laradock/mysql/my.cnf

Add the last line of the file.

![](README_images/6.png)

	default authentication plugin=mysql_native_password

Now we enter the **Mysql** container, give the user permissions, and create the database.

	sudo docker-compose exec mysql bash
	root@c2772502897c:/# mysql -u root -p

_(password = **root**)_

	mysql> ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';
	mysql> ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'root';
	mysql> ALTER USER 'default'@'%' IDENTIFIED WITH mysql_native_password BY 'secret';
	mysql> create database technical_test;
	mysql> show databases;

Configure the `.env` file of **Laravel** to enter the database connection information. Disconnect from **MySQL** and exit the container.

	mysql> exit
	root@c9525b8e5105:/# exit
	$ cd ../technical_test/
	$ nano .env￼

Configure `.env` of **Laravel** to use the data we created

![](README_images/7.png)

Project `.env` file

	DB_CONNECTION=mysql
	DB_HOST=mysql
	DB_PORT=3306
	DB_DATABASE=technical_test
	DB_USERNAME=root
	DB_PASSWORD=root

**laradock** `.env` file

	MYSQL_VERSION=latest
	MYSQL_DATABASE=technical_test
	MYSQL_USER=root
	MYSQL_PASSWORD=root
	MYSQL_PORT=3306
	MYSQL_ROOT_PASSWORD=root

Restart the containers

	sudo docker-compose down
	sudo docker-compose up -d nginx mysql workspace phpmyadmin redis

Test

	sudo docker-compose --build nginx mysql phpmyadmin

Run migrations
----

	sudo docker-compose exec workspace bash
	artisan migrate

Access phpMyAdmin
----

	http://localhost:8080/
* host -> **mysql**
* user -> **root**
* password -> **root**

Run tests with PHPUnit
----
Connect to the **Docker** machine where the server is running

	sudo docker-compose exec workspace bash
	
And run **PHPUnit**

![](README_images/8.png)