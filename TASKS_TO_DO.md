## Technical Test

The test is composed of 4 modules that will be evaluated individually.

The main requirements established are the use of Laravel and performing unit tests.

All complementary technologies that the candidate needs to satisfy the requirements may be used.

The MovieDB API is provided for the test.

https://www.themoviedb.org/documentation/api

The duration of the test will be a maximum of 4 days from delivery to the candidate.

The use of design patterns as well as clean code methodologies will be valued.

The project code must be uploaded to a Git repository and the URL sent to us.

Module 1:
----

Create a docker with the necessary containers to run the application.

Module 2:
----

Create a web interface in which a user can see the top 100 movies by vote, these will be paginated every 20 movies (In the background, the information of the 100 movies will be cached using redis) so that when the user links to one of them, it shows basic information:

Module 3:
----

Name
- Year
- Director
- Description
- Photo
etc...

The user can create a profile to store their favorite movies.

Once the user logs in to the application, they will have access to the list of previously saved movies ordered by average rating according to votes received.

Module 4:
----

A Top Movies button will be created on the home of the website, which will filter the movies according to the following conditions:

The movies that have an average of more than 4 stars, do not have more than 10% of negative votes in 2019, and do not feature Keanu Reeves will be displayed.
(This filter must be made with Eloquent Laravel).